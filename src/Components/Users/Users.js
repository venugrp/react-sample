import React, {useState}  from 'react';
import './Users.css';
import moment from 'moment';
import AppModal from './../Modal/Modal';

const Users = (props) => {

    const [show, setShow] = useState(false);
    const [user, setUser] = useState({});

    const handleShow = (user) => {
      setUser(user);
      setShow(true);
    }

    const handleModal = (e) => {
      if(e === 'yes') {
        props.onDelete(user);
        setShow(false);
      }else {
        setUser({});
        setShow(false);
      }
    }

    if (props.items && props.items.length) {
      let users = props.items.map((item, i) => {
          return(
            <tr key={item.id}>
            <th scope="row">{i+1}</th>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{item.resumeUrl}</td>
            <td>{item.createdAt ? moment(item.createdAt).format('lll') :  "-"}</td>
            <td><button className="btn btn-sm btn-danger" onClick={() => handleShow(item)}>Delete</button></td>
            </tr>
          )
      })
      return(
        <div id="style-1" className="scrollbar">
            <table className="table table-bordered table-striped">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Resume Url</th>
                <th scope="col">Created</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                {users}
            </tbody>
            </table>
            <AppModal isShow={show} user={user} onModal={(e) => handleModal(e)} />
        </div>
        
      )
    }
    return null;
}

export default Users;