import React from 'react';
import './Modal.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const AppModal = (props) => {
    
    return (
      <>
        <Modal show={props.isShow}  onHide={() =>props.onModal('no')} centered>
          <Modal.Header closeButton>
            <Modal.Title>Confirmation</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete {props.user.name} ?</Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() =>props.onModal('no')}>
              No
            </Button>
            <Button variant="danger" onClick={() =>props.onModal('yes')}>
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
}

export default AppModal;