import React from 'react';
import './ValidationErrors.css';

const ValidationErrors = (props) => {
    if (!props.valid) {
      return(
        <div className='error-msg'>{props.message}</div>
      )
    }
    return null;
}

export default ValidationErrors;