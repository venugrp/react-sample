import React, { Component } from 'react';
import axios from 'axios';
import ValidationErrors from './Components/ValidationErrors/ValidationErrors';
import validations from './js/validations';
import Users from './Components/Users/Users';
import Loader from './Components/Loader/Loader';
import config from './config/config';
import {ToastsContainer, ToastsStore} from 'react-toasts';
import Header from './Components/Header/Header';
import './App.css';

class App extends Component {
  state = {
    items: [],
    userForm: {
      firstName: {
        value: '',
        isValid: true,
        errorMessage: ''
      },
      lastName: {
        value: ''
      },
      email: {
        value: '',
        isValid: true,
        errorMessage: ''
      },
      resumeUrl: {
        value: '',
        isValid: true,
        errorMessage: ''
      },
      isValid: false
    },
    isLoading: false
  }

  constructor(props) {
    super(props);
    //create ref
    this.userFormRef = React.createRef();
  }

  componentDidMount() {
    this.setState({ isLoading: true });
    axios.get(`${config.apiBaseUrl}/application`)
      .then(res => {
        const items = res.data;
        this.setState({ isLoading: false })
        this.setState({ items });
      }).catch((err) => {
        this.setState({ isLoading: false })
        console.log("Error in fetching records", err);
      })
  }

  updateEmail = (value) => {
    const formData = this.state.userForm;
    const { email } = formData;
    email.value = value;
    formData.email = email;
    this.setState({ userForm: formData }, this.validateEmail)
  }

  updateUrl = (value) => {
    const formData = this.state.userForm;
    const { resumeUrl } = formData;
    resumeUrl.value = value;
    formData.resumeUrl = resumeUrl;
    this.setState({ userForm: formData }, this.validateUrl)
  }

  updateName = (value, type) => {
    const formData = this.state.userForm;
    const { firstName, lastName } = formData;
    if (type == 'first') {
      firstName.value = value;
      formData.firstName = firstName;
      this.setState({ userForm: formData }, this.validateName)
    } else if (type == 'last') {
      lastName.value = value;
      formData.lastName = lastName;
      this.setState({ userForm: formData })
    }

  }

  validateEmail = () => {
    const formData = this.state.userForm;
    const { email } = formData;
    email.isValid = true;
    if (!email.value) {
      email.isValid = false;
      email.errorMessage = 'Email is required'
    }
    else if (!validations.validateEmail(email.value)) {
      email.isValid = false;
      email.errorMessage = 'Email format incorrect'
    }
    formData.email = email;
    this.setState({ userForm: formData }, this.validateForm)
  }

  validateUrl = () => {
    const formData = this.state.userForm;
    const { resumeUrl } = formData;
    resumeUrl.isValid = true;
    if (!resumeUrl.value) {
      resumeUrl.isValid = false;
      resumeUrl.errorMessage = 'Resume url is required'
    }
    else if (!validations.validateUrl(resumeUrl.value)) {
      resumeUrl.isValid = false;
      resumeUrl.errorMessage = 'Url format incorrect';
    }
    formData.resumeUrl = resumeUrl;
    this.setState({ userForm: formData }, this.validateForm)
  }

  validateName = () => {
    const formData = this.state.userForm;
    const { firstName } = formData;
    firstName.isValid = true;
    if (!firstName.value) {
      firstName.isValid = false;
      firstName.errorMessage = 'First name is required'
    }
    formData.firstName = firstName;
    this.setState({ userForm: formData }, this.validateForm)
  }

  validateForm = () => {
    const formData = { ...this.state.userForm };
    formData.isValid = formData.firstName.isValid && formData.email.isValid && formData.resumeUrl.isValid;
    this.setState({ userForm: formData });
  }

  showSuccessMsg = (msg) => {
    ToastsStore.success(msg)
  }
  showErrorMsg = (msg) => {
    ToastsStore.error(msg)
  }
   

  deleteUser = (user) => {
    let items = [...this.state.items];
    this.setState({ isLoading: true });
    axios.delete(`${config.apiBaseUrl}/application/${user.id}`)
      .then(res => {
        console.log(res);
        this.setState({ isLoading: false });
        items = items.filter((item) => {
          return item.id != user.id;
        })
        this.setState({ items });
        this.showSuccessMsg('User deleted successfully..')
      }).catch((err) => {
        this.setState({ isLoading: false })
        console.log("Error in Post user", err);
        this.showErrorMsg('Opps! Something went wrong..')
      })
  }

  submitData() {
    const formData = { ...this.state.userForm };
    let items = [...this.state.items]
    let name = formData.firstName.value;
    name = formData.lastName.value ? `${name} ${formData.lastName.value}` : name;
    let user = {
      email: formData.email.value,
      name,
      resumeUrl: formData.resumeUrl.value,
    }
    this.setState({ isLoading: true });
    axios.post(`${config.apiBaseUrl}/application`, user)
      .then(res => {
        this.setState({ isLoading: false });
        this.userFormRef.reset();
        items.push(res.data);
        this.setState({ items });
        this.showSuccessMsg('User added successfully..');
        const formData = { ...this.state.userForm };
        formData.isValid = false;
        this.setState({ userForm: formData });
      }).catch((err) => {
        this.setState({ isLoading: false })
        console.log("Error in Post user", err);
        this.showErrorMsg('Opps! Something went wrong..')
      })
  }

  render() {
    let listLoader = this.state.isLoading ? <Loader /> : null;
    return (
      <>
        <Header />
        <div className="container">
          <div className="formDiv">
            <form className="form inputgrp" autoComplete="off" ref={(el) => this.userFormRef = el}>
              <div className="form-group">
                <label className="inputLabel" htmlFor="firstName">First Name:</label>
                <input type="text" className={`form-control ${!this.state.userForm.firstName.isValid ? 'input-error': '' }`} id="firstName" onChange={(e) => this.updateName(e.target.value, 'first')} onBlur={(e) => this.updateName(e.target.value, 'first')} />
                <ValidationErrors valid={this.state.userForm.firstName.isValid} message={this.state.userForm.firstName.errorMessage} />
              </div>
              <div className="form-group">
                <label className="inputLabel" htmlFor="lastName">Last Name:</label>
                <input type="text" className="form-control" id="lastName" onChange={(e) => this.updateName(e.target.value, 'last')} />
              </div>
              <div className="form-group">
                <label className="inputLabel" htmlFor="email">Email:</label>
                <input type="email" className={`form-control ${!this.state.userForm.email.isValid ? 'input-error': '' }`} id="email" onChange={(e) => this.updateEmail(e.target.value)} onBlur={(e) => this.updateEmail(e.target.value)} />
                <ValidationErrors valid={this.state.userForm.email.isValid} message={this.state.userForm.email.errorMessage} />
              </div>

              <div className="form-group">
                <label className="inputLabel" htmlFor="resumeUrl">Resume Url:</label>
                <input type="text" className={`form-control ${!this.state.userForm.resumeUrl.isValid ? 'input-error': '' }`} id="resumeUrl" onChange={(e) => this.updateUrl(e.target.value)} onBlur={(e) => this.updateUrl(e.target.value)}/>
                <ValidationErrors valid={this.state.userForm.resumeUrl.isValid} message={this.state.userForm.resumeUrl.errorMessage} />
              </div>
              <button type="button" className={`btn btn-success btn-custom ${!this.state.userForm.isValid ? 'disableBtn': ''}`} disabled={!this.state.userForm.isValid} onClick={() => this.submitData()}>Submit</button>
            </form>
          </div>
          <div className="listCls">
            {listLoader}
            <Users items={this.state.items} onDelete={(e) => this.deleteUser(e)}></Users>
          </div>
          <ToastsContainer store={ToastsStore}/>
        </div>
      </>

    );
  }
}

export default App;
