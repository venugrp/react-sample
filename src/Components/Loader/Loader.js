import React from 'react';
import './Loader.css';

const Loader = () => {
    return(
        <div className="d-flex justify-content-center customLoader">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
    
}

export default Loader;