import React from 'react';
import './Header.css';

const Header = () => {
    return(
        <div className="header">
          <img className="headerImg" alt="React Logo" src='/logo192.png' width="65px" height="60px"></img>
          <div className='headText'>
            React Sample
          </div>
        </div>
    )
    
}

export default Header;